<?php

namespace Drupal\layout_block\Entity\BlockContent;

trait LayoutBlockTrait {

  public function getAccessDependency() {
    /** @var \Drupal\layout_builder\InlineBlockUsage $inline_block_usage */
    $inline_block_usage = \Drupal::service('inline_block.usage');
    $hasUsage = $inline_block_usage->getUsage($this->id());
    if ($hasUsage) {
      return \Drupal::entityTypeManager()->getStorage($hasUsage->layout_entity_type)->load($hasUsage->layout_entity_id);
    }
  }

}
