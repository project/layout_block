<?php

namespace Drupal\layout_block\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder_at\Plugin\Field\FieldWidget\LayoutBuilderCopyWidget;

/**
 * Extended layout_builder_at widget to support our custom block
 *
 * @FieldWidget(
 *   id = "layout_block_layout_builder_at_copy",
 *   label = @Translation("Layout Builder Asymmetric Translation (layout_block support)"),
 *   description = @Translation("A field widget for Layout Builder. This exposes a checkbox on the entity form to copy the blocks on translation. Adds support for layout_block"),
 *   field_types = {
 *     "layout_section",
 *   },
 *   multiple_values = FALSE,
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
class LayoutBlockLayoutBuilderCopyWidget extends LayoutBuilderCopyWidget {

  /**
   * Extract form values.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state): void {
    // Need to bail out on the same conditions as the parent function.
    // So the below "return;" are just plain copies.
    // ----- START COPY -----
    // @todo This isn't resilient to being set twice, during validation and
    //   save https://www.drupal.org/project/drupal/issues/2833682.
    if (!$form_state->isValidationComplete()) {
      return;
    }

    $field_name = $this->fieldDefinition->getName();

    // We can only copy if the field is set and access is TRUE.
    if (isset($form[$field_name]['widget']['#layout_builder_at_access']) && !$form[$field_name]['widget']['#layout_builder_at_access']) {
      return;
    }
    // ----- END COPY -----

    // Run parent
    parent::extractFormValues($items, $form, $form_state);

    // Get the new sections and clone our layout blocks.
    $new_sections = $items->getValue();
    if (empty($new_sections) || !is_array($new_sections)) {
      return;
    }
    foreach ($new_sections as $delta => $section) {
      $new_sections[$delta]['section'] = _layout_block_translate_helper($section['section'], $items->getEntity()->language()->getId());
    }
    $items->setValue($new_sections);
  }

}
