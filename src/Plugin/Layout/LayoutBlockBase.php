<?php

namespace Drupal\layout_block\Plugin\Layout;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class LayoutBlockBase extends LayoutDefault implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager|object|null
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Define a block_content bundle name to be used for this layout.
   *
   * @return string The block_content bundle name.
   */
  abstract protected function getBlockBundle(): string;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $configuration['label'] = '(' . $this->getPluginDefinition()->getLabel() . ') ';
    // Block should only be used to handle block_usages.
    $configuration['block'] = NULL;
    $configuration['block_revision'] = NULL;
    $configuration['block_serialized'] = NULL;
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // BC layer code for blocks that were not stored with a revision.
    $this->configuration = layout_block_bc_layer_helper($this->configuration);

    $block = NULL;
    if (isset($this->configuration['block_serialized']) && !empty($this->configuration['block_serialized'])) {
      $block = unserialize($this->configuration['block_serialized']);
    }
    elseif (isset($this->configuration['block_revision']) && is_numeric($this->configuration['block_revision'])) {
      $block = $this->entityTypeManager->getStorage('block_content')->loadRevision($this->configuration['block_revision']);
    }
    $form['block'] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'block_content',
      '#bundle' => $this->getBlockBundle(),
      '#form_mode' => 'default',
      '#default_value' => $block,
      // Prevent blocks from immediately saving.
      // The published revision of the page references a block id and that would lead to:
      //    1/ changes becoming immediately visible
      //    2/ revisions not working correctly
      // We will store the block internally (on submit) as a serialized object and then save that once the page itself is saved.
      //    @see: layout_block_entity_presave().
      '#save_entity' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    /** @var \Drupal\block_content\Entity\BlockContent $block */
    $block = &$form['block']['#entity'];

    // Keep the label for the block in sync with the label for this layout.
    $layout_label = $form_state->getValue('label');
    $block->set('info', $layout_label);

    // Mark block as non-reusable.
    $block->setNonReusable();

    // Store the block as a serialized object until the page save will trigger the actual block save.
    // For new blocks the block id and revision will simply return NULL.
    $this->configuration['block'] = $block->id();
    $this->configuration['block_revision'] = $block->getRevisionId();
    $this->configuration['block_serialized'] = serialize($block);
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    // BC layer code for blocks that were not stored with a revision.
    $this->configuration = layout_block_bc_layer_helper($this->configuration);

    // Load our block containing all config and pass it to the build array.
    if (isset($this->configuration['block_serialized']) && !empty($this->configuration['block_serialized'])) {
      $block = unserialize($this->configuration['block_serialized']);
    }
    else {
      $block = $this->entityTypeManager->getStorage('block_content')->loadRevision($this->configuration['block_revision']);
    }

    // In some rare cases, the block might not be available anymore.
    // Usually occurs when dealing with default content in CT's between different environments.
    // Things will be broken at this point and there is no way to make it work.
    // So instead of throwing a fatal error, locking the content editor out, we will just return the $build array as-is.
    if (!$block instanceof BlockContent) {
      return $build;
    }

    // Trick backend (LB) into rendering the block, if it is not reusable it won't render?
    // Just make sure to never save this in database.
    $block->set('reusable', TRUE);

    // Merge in cache tags and contexts from the block.
    if (!isset($build['#cache']['tags']) || !is_array($build['#cache']['tags'])) {
      $build['#cache']['tags'] = [];
    }
    $build['#cache']['tags'] = array_merge(
      $build['#cache']['tags'],
      $block->getCacheTags(),
    );
    if (!isset($build['#cache']['contexts']) || !is_array($build['#cache']['contexts'])) {
      $build['#cache']['contexts'] = [];
    }
    $build['#cace']['contexts'] = array_merge(
      $build['#cache']['contexts'],
      $block->getCacheContexts(),
    );

    // The block view access determines if the layout should is visible.
    // This will allow published flag and more advanced access systems from the
    // block_content to determine if the layout should be rendered.
    $build['#access'] = $block->access('view');

    // Set block in the build for easy access in the template.
    $build['#block'] = $block;

    return $build;
  }

}
