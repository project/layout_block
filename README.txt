1/
Create a custom layout

2/
Create a custom block_content type for your layout (use 1 block type per layout)

3/
Create a layout class for your custom layout and extend "\Drupal\layout_block\Plugin\Layout\LayoutBlockBase".
You will need to implement the function "protected function getBlockBundle(): string" and make it return your block bundle name.

You can now add fields to your block and logic to your layout.

4/
Implement a class per block type in a custom module:
  /**
   * Implements hook_entity_bundle_info_alter().
   */
  function MYMODULE_entity_bundle_info_alter(array &$bundles): void {
    if (isset($bundles['block_content']['MY_LAYOUT_BLOCK'])) {
      $bundles['block_content']['MY_LAYOUT_BLOCK']['class'] = \Drupal\MYMODULE\Entity\Block\MYLAYOUTBLOCK::class;
    }
  }

5/
Use the "LayoutBlockTrait" in your block class.
