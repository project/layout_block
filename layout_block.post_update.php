<?php

/**
 * Fix all layout_blocks in the section_library that might reference a block that is also used on a page.
 */
function layout_block_post_update_9001(&$sandbox = NULL) {
  $moduleHandler = \Drupal::service('module_handler');
  if (!$moduleHandler->moduleExists('section_library')) {
    return;
  }

  $ids = \Drupal::entityQuery('section_library_template')
    ->accessCheck(FALSE)
    ->execute();
  foreach (\Drupal::entityTypeManager()->getStorage('section_library_template')->loadMultiple($ids) as $entity) {
    /** @var \Drupal\section_library\Entity\SectionLibraryTemplate $entity */
    $values = $entity->get('layout_section')->getValue();
    if (!is_array($values)) {
      continue;
    }
    foreach ($values as $value_index => $value) {
      if (!is_array($value)) {
        continue;
      }
      foreach ($value as $section_index => $section) {
        if (!$section instanceof \Drupal\layout_builder\Section) {
          continue;
        }
        // Parse all sections through our clone helper function, so they all get converted to serialized blocks.
        $section = _layout_block_clone_helper($section);
        $values[$value_index][$section_index] = $section;
      }
    }
    $entity->set('layout_section', $values);
    $entity->save();
  }
}
